#(define (zccidental-id grob)

	(format-id grob '"ly grob zccidental")

	)
#(define (accidentalcautionary-id grob)

	(format-id grob '"ly grob accidentalcautionary")

	)
#(define (accidentalplacement-id grob)

	(format-id grob '"ly grob accidentalplacement")

	)
#(define (accidentalsuggestion-id grob)

	(format-id grob '"ly grob accidentalsuggestion")

	)
#(define (ambitus-id grob)

	(format-id grob '"ly grob ambitus")

	)
#(define (ambitusaccidental-id grob)

	(format-id grob '"ly grob ambitusaccidental")

	)
#(define (ambitusline-id grob)

	(format-id grob '"ly grob ambitusline")

	)
#(define (ambitusnotehead-id grob)

	(format-id grob '"ly grob ambitusnotehead")

	)
#(define (arpeggio-id grob)

	(format-id grob '"ly grob arpeggio")

	)
#(define (balloontextitem-id grob)

	(format-id grob '"ly grob balloontextitem")

	)
#(define (barline-id grob)

	(format-id grob '"ly grob barline")

	)
#(define (barnumber-id grob)

	(format-id grob '"ly grob barnumber")

	)
#(define (bassfigure-id grob)

	(format-id grob '"ly grob bassfigure")

	)
#(define (bassfigurealignment-id grob)

	(format-id grob '"ly grob bassfigurealignment")

	)
#(define (bassfigurealignmentpositioning-id grob)

	(format-id grob '"ly grob bassfigurealignmentpositioning")

	)
#(define (bassfigurebracket-id grob)

	(format-id grob '"ly grob bassfigurebracket")

	)
#(define (bassfigurecontinuation-id grob)

	(format-id grob '"ly grob bassfigurecontinuation")

	)
#(define (bassfigureline-id grob)

	(format-id grob '"ly grob bassfigureline")

	)
#(define (beam-id grob)

	(format-id grob '"ly grob beam")

	)
#(define (bendafter-id grob)

	(format-id grob '"ly grob bendafter")

	)
#(define (breakaligngroup-id grob)

	(format-id grob '"ly grob breakaligngroup")

	)
#(define (breakalignment-id grob)

	(format-id grob '"ly grob breakalignment")

	)
#(define (breathingsign-id grob)

	(format-id grob '"ly grob breathingsign")

	)
#(define (chordname-id grob)

	(format-id grob '"ly grob chordname")

	)
#(define (clef-id grob)

	(format-id grob '"ly grob clef")

	)
#(define (clefmodifier-id grob)

	(format-id grob '"ly grob clefmodifier")

	)
#(define (clusterspanner-id grob)

	(format-id grob '"ly grob clusterspanner")

	)
#(define (clusterspannerbeacon-id grob)

	(format-id grob '"ly grob clusterspannerbeacon")

	)
#(define (combinetextscript-id grob)

	(format-id grob '"ly grob combinetextscript")

	)
#(define (cueclef-id grob)

	(format-id grob '"ly grob cueclef")

	)
#(define (cueendclef-id grob)

	(format-id grob '"ly grob cueendclef")

	)
#(define (custos-id grob)

	(format-id grob '"ly grob custos")

	)
#(define (dotcolumn-id grob)

	(format-id grob '"ly grob dotcolumn")

	)
#(define (dots-id grob)

	(format-id grob '"ly grob dots")

	)
#(define (doublepercentrepeat-id grob)

	(format-id grob '"ly grob doublepercentrepeat")

	)
#(define (doublepercentrepeatcounter-id grob)

	(format-id grob '"ly grob doublepercentrepeatcounter")

	)
#(define (doublerepeatslash-id grob)

	(format-id grob '"ly grob doublerepeatslash")

	)
#(define (dynamiclinespanner-id grob)

	(format-id grob '"ly grob dynamiclinespanner")

	)
#(define (dynamictext-id grob)

	(format-id grob '"ly grob dynamictext")

	)
#(define (dynamictextspanner-id grob)

	(format-id grob '"ly grob dynamictextspanner")

	)
#(define (episema-id grob)

	(format-id grob '"ly grob episema")

	)
#(define (fingering-id grob)

	(format-id grob '"ly grob fingering")

	)
#(define (fingeringcolumn-id grob)

	(format-id grob '"ly grob fingeringcolumn")

	)
#(define (flag-id grob)

	(format-id grob '"ly grob flag")

	)
#(define (footnoteitem-id grob)

	(format-id grob '"ly grob footnoteitem")

	)
#(define (footnotespanner-id grob)

	(format-id grob '"ly grob footnotespanner")

	)
#(define (fretboard-id grob)

	(format-id grob '"ly grob fretboard")

	)
#(define (glissando-id grob)

	(format-id grob '"ly grob glissando")

	)
#(define (gracespacing-id grob)

	(format-id grob '"ly grob gracespacing")

	)
#(define (gridline-id grob)

	(format-id grob '"ly grob gridline")

	)
#(define (gridpoint-id grob)

	(format-id grob '"ly grob gridpoint")

	)
#(define (hairpin-id grob)

	(format-id grob '"ly grob hairpin")

	)
#(define (horizontalbracket-id grob)

	(format-id grob '"ly grob horizontalbracket")

	)
#(define (instrumentname-id grob)

	(format-id grob '"ly grob instrumentname")

	)
#(define (instrumentswitch-id grob)

	(format-id grob '"ly grob instrumentswitch")

	)
#(define (keycancellation-id grob)

	(format-id grob '"ly grob keycancellation")

	)
#(define (keysignature-id grob)

	(format-id grob '"ly grob keysignature")

	)
#(define (kievanligature-id grob)

	(format-id grob '"ly grob kievanligature")

	)
#(define (laissezvibrertie-id grob)

	(format-id grob '"ly grob laissezvibrertie")

	)
#(define (laissezvibrertiecolumn-id grob)

	(format-id grob '"ly grob laissezvibrertiecolumn")

	)
#(define (ledgerlinespanner-id grob)

	(format-id grob '"ly grob ledgerlinespanner")

	)
#(define (leftedge-id grob)

	(format-id grob '"ly grob leftedge")

	)
#(define (ligaturebracket-id grob)

	(format-id grob '"ly grob ligaturebracket")

	)
#(define (lyricextender-id grob)

	(format-id grob '"ly grob lyricextender")

	)
#(define (lyrichyphen-id grob)

	(format-id grob '"ly grob lyrichyphen")

	)
#(define (lyricspace-id grob)

	(format-id grob '"ly grob lyricspace")

	)
#(define (lyrictext-id grob)

	(format-id grob '"ly grob lyrictext")

	)
#(define (measurecounter-id grob)

	(format-id grob '"ly grob measurecounter")

	)
#(define (measuregrouping-id grob)

	(format-id grob '"ly grob measuregrouping")

	)
#(define (melodyitem-id grob)

	(format-id grob '"ly grob melodyitem")

	)
#(define (mensuralligature-id grob)

	(format-id grob '"ly grob mensuralligature")

	)
#(define (metronomemark-id grob)

	(format-id grob '"ly grob metronomemark")

	)
#(define (multimeasurerest-id grob)

	(format-id grob '"ly grob multimeasurerest")

	)
#(define (multimeasurerestnumber-id grob)

	(format-id grob '"ly grob multimeasurerestnumber")

	)
#(define (multimeasureresttext-id grob)

	(format-id grob '"ly grob multimeasureresttext")

	)
#(define (nonmusicalpapercolumn-id grob)

	(format-id grob '"ly grob nonmusicalpapercolumn")

	)
#(define (notecollision-id grob)

	(format-id grob '"ly grob notecollision")

	)
#(define (notecolumn-id grob)

	(format-id grob '"ly grob notecolumn")

	)
#(define (notehead-id grob)

	(format-id grob '"ly grob notehead")

	)
#(define (notename-id grob)

	(format-id grob '"ly grob notename")

	)
#(define (notespacing-id grob)

	(format-id grob '"ly grob notespacing")

	)
#(define (ottavabracket-id grob)

	(format-id grob '"ly grob ottavabracket")

	)
#(define (papercolumn-id grob)

	(format-id grob '"ly grob papercolumn")

	)
#(define (parenthesesitem-id grob)

	(format-id grob '"ly grob parenthesesitem")

	)
#(define (percentrepeat-id grob)

	(format-id grob '"ly grob percentrepeat")

	)
#(define (percentrepeatcounter-id grob)

	(format-id grob '"ly grob percentrepeatcounter")

	)
#(define (phrasingslur-id grob)

	(format-id grob '"ly grob phrasingslur")

	)
#(define (pianopedalbracket-id grob)

	(format-id grob '"ly grob pianopedalbracket")

	)
#(define (rehearsalmark-id grob)

	(format-id grob '"ly grob rehearsalmark")

	)
#(define (repeatslash-id grob)

	(format-id grob '"ly grob repeatslash")

	)
#(define (repeattie-id grob)

	(format-id grob '"ly grob repeattie")

	)
#(define (repeattiecolumn-id grob)

	(format-id grob '"ly grob repeattiecolumn")

	)
#(define (rest-id grob)

	(format-id grob '"ly grob rest")

	)
#(define (restcollision-id grob)

	(format-id grob '"ly grob restcollision")

	)
#(define (script-id grob)

	(format-id grob '"ly grob script")

	)
#(define (scriptcolumn-id grob)

	(format-id grob '"ly grob scriptcolumn")

	)
#(define (scriptrow-id grob)

	(format-id grob '"ly grob scriptrow")

	)
#(define (slur-id grob)

	(format-id grob '"ly grob slur")

	)
#(define (sostenutopedal-id grob)

	(format-id grob '"ly grob sostenutopedal")

	)
#(define (sostenutopedallinespanner-id grob)

	(format-id grob '"ly grob sostenutopedallinespanner")

	)
#(define (spacingspanner-id grob)

	(format-id grob '"ly grob spacingspanner")

	)
#(define (spanbar-id grob)

	(format-id grob '"ly grob spanbar")

	)
#(define (spanbarstub-id grob)

	(format-id grob '"ly grob spanbarstub")

	)
#(define (staffgrouper-id grob)

	(format-id grob '"ly grob staffgrouper")

	)
#(define (staffspacing-id grob)

	(format-id grob '"ly grob staffspacing")

	)
#(define (staffsymbol-id grob)

	(format-id grob '"ly grob staffsymbol")

	)
#(define (stanzanumber-id grob)

	(format-id grob '"ly grob stanzanumber")

	)
#(define (stem-id grob)

	(format-id grob '"ly grob stem")

	)
#(define (stemstub-id grob)

	(format-id grob '"ly grob stemstub")

	)
#(define (stemtremolo-id grob)

	(format-id grob '"ly grob stemtremolo")

	)
#(define (stringnumber-id grob)

	(format-id grob '"ly grob stringnumber")

	)
#(define (strokefinger-id grob)

	(format-id grob '"ly grob strokefinger")

	)
#(define (sustainpedal-id grob)

	(format-id grob '"ly grob sustainpedal")

	)
#(define (sustainpedallinespanner-id grob)

	(format-id grob '"ly grob sustainpedallinespanner")

	)
#(define (system-id grob)

	(format-id grob '"ly grob system")

	)
#(define (systemstartbar-id grob)

	(format-id grob '"ly grob systemstartbar")

	)
#(define (systemstartbrace-id grob)

	(format-id grob '"ly grob systemstartbrace")

	)
#(define (systemstartbracket-id grob)

	(format-id grob '"ly grob systemstartbracket")

	)
#(define (systemstartsquare-id grob)

	(format-id grob '"ly grob systemstartsquare")

	)
#(define (tabnotehead-id grob)

	(format-id grob '"ly grob tabnotehead")

	)
#(define (textscript-id grob)

	(format-id grob '"ly grob textscript")

	)
#(define (textspanner-id grob)

	(format-id grob '"ly grob textspanner")

	)
#(define (tie-id grob)

	(format-id grob '"ly grob tie")

	)
#(define (tiecolumn-id grob)

	(format-id grob '"ly grob tiecolumn")

	)
#(define (timesignature-id grob)

	(format-id grob '"ly grob timesignature")

	)
#(define (trillpitchaccidental-id grob)

	(format-id grob '"ly grob trillpitchaccidental")

	)
#(define (trillpitchgroup-id grob)

	(format-id grob '"ly grob trillpitchgroup")

	)
#(define (trillpitchhead-id grob)

	(format-id grob '"ly grob trillpitchhead")

	)
#(define (trillspanner-id grob)

	(format-id grob '"ly grob trillspanner")

	)
#(define (tupletbracket-id grob)

	(format-id grob '"ly grob tupletbracket")

	)
#(define (tupletnumber-id grob)

	(format-id grob '"ly grob tupletnumber")

	)
#(define (unacordapedal-id grob)

	(format-id grob '"ly grob unacordapedal")

	)
#(define (unacordapedallinespanner-id grob)

	(format-id grob '"ly grob unacordapedallinespanner")

	)
#(define (vaticanaligature-id grob)

	(format-id grob '"ly grob vaticanaligature")

	)
#(define (verticalalignment-id grob)

	(format-id grob '"ly grob verticalalignment")

	)
#(define (verticalaxisgroup-id grob)

	(format-id grob '"ly grob verticalaxisgroup")

	)
#(define (voicefollower-id grob)

	(format-id grob '"ly grob voicefollower")

	)
#(define (voltabracket-id grob)

	(format-id grob '"ly grob voltabracket")

	)
#(define (voltabracketspanner-id grob)

	(format-id grob '"ly grob voltabracketspanner")

	)
